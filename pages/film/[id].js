import { useEffect } from "react";
import { API_BASE_URL, API_TOKEN, API_LANG } from "../../config";
import moment from "moment";
import Layout from "../../components/Layout";
import axios from "axios";

/**
 * chargement des donnée SSR
 * @param {object} context
 */
export async function getServerSideProps(context) {
  const { id } = context.query

  const url = `${API_BASE_URL}movie/${id}?api_key=${API_TOKEN}&language=${API_LANG}`

  const res = await axios.get(url)

  return {
    props : {
      film: res.data
    }
  };
};

/**
 * FilmDetail, page d'affichage des détailles des films
 */
const FilmDetail = props =>  {
  const { film } = props
  useEffect(() => {
		/* eslint-disable-next-line no-undef */
		document.title = film.original_title
	},[]);
  return (
    <Layout>
      <div className="py-3">
        <div className="row">
          <div className="col-10 mx-auto text-center text-slanted text-blue my-5">
            <h1>{film.original_title}</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-10 mx-auto col-md-4 my-3">
            <img src= {film.poster_path ? `https://image.tmdb.org/t/p/w300${film.poster_path}` : `https://via.placeholder.com/350x525?text=Pas%20d%27image`} className="img-fluid w-100" alt={`${film.original_title}`} />
          </div>
          <div className="col-10 mx-auto col-md-8 my-3">
            <h4>Date de sortie : <small className="text-muted">{`${moment(film.release_date).format("DD/MM/YYYY")}`}</small></h4>
            <h4 className="mt-3 mb-2">
                <span>Genre : </span> 
                {film.genres && 
                    film.genres.map((item, index) => {
                        
                        return(
                            <small className="text-muted" key={index}>{(index ? ', ' : '') + item.name}</small>
                        )
                    })
                }
            </h4>
            <h4 className="mt-3 mb-2">
              Déscription :  
              { film.overview && 
                <small className="text-muted"> { film.overview } </small>
              }
            </h4>
            <h4>Note : <small className="text-muted">{`${film.vote_average}`}</small></h4>
            <h4><span>Production : </span>
              { film.production_companies &&
                film.production_companies.map((item, index) => {
                  return(
                    <small key={index} className="text-muted">{(index ? ', ' : '') + item.name}</small>
                  )
                })
              }
            </h4>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default FilmDetail