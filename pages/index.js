import React from "react";
import { API_BASE_URL, API_TOKEN, API_LANG } from "../config";
import axios from "axios";
import Layout from '../components/Layout';
import FilmItem from '../components/FilmItem';
import Router from 'next/router';


/**
 * getServerSideProps : chargement des données SSR
 * API_BASE_URL : url de base de ws
 * API_TOKEN : clé de sécurité de ws
 * API_LANG langue : de ws
 * page : numéro de page 
 */
export async function getServerSideProps({ query: { page = 1, filter = 'all' } }) {
  
  let url = `${API_BASE_URL}discover/movie?api_key=${API_TOKEN}&language=${API_LANG}&page=${page}`
  
  if (filter !== 'all'){ url += `&with_genres=${filter}` }
  
  const res = await axios.get(url)

  return {
    props : {
      films: res.data.results,
      page: parseInt(page, 10),
      filter
    }
  };
}

/**
 * Affichage de liste des films
 * films : liste des films
 * page : numéro de page
 * filter : id de genre des films
 */
const Index = props => {
  const {films, page, filter} = props

  /**
   * _onFilterChange, fonction de filtrage des données 
   * @param {object} e évenement 
   */
  const _onFilterChange = async (e) => {
    Router.push(`/?page=1&filter=${e.target.value}`)
  }

  /**
   * _onPageChange : fonction de pagination
   * @param {number} page 
   */
  const _onPageChange = async (page) => {
    Router.push(`/?page=${page}&filter=${filter}`)
  }

  return(
    <Layout>
      <div className="py-5">
        <div className="container">

        <select onChange={(e) => _onFilterChange(e)}>
              <option value="all">Aucun filtre</option>
              <option value="18">Genre 1</option>
              <option value="35">Genre 2</option>
              <option value="53">Genre 3</option>
            </select>

          <div className="row">

            {films && 
              films.map((film, index) =>(
                <FilmItem film = {film} key={index} />
              ))
            }
          </div>
          <div className="d-flex flex-row justify-content-end mt-9">
            <button
              className="btn btn-primary mr-2 p-2"
              onClick={() => _onPageChange(page-1)}
              disabled={page <= 1}
            >
              Précédent 
            </button>
            <button 
              className="btn btn-primary p-2"
              onClick={() => _onPageChange(page+1)}
            >
              Suivant
            </button>        
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default Index