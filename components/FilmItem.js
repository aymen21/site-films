import Link from "next/link";
/**
 * bloc d'affichage de chaque films dans la liste  
 */
const FilmItem = props => {
      const film = props.film;
      return (
        <div className="col-9 col-md-6 col-lg-4 my-3">
          <div className="card">
            <div className="p-2">
              <Link href={`/film/${[film.id]}`}>
                <img
                  src= {film.poster_path ? `https://image.tmdb.org/t/p/w300${film.poster_path}` : `https://via.placeholder.com/322x488?text=Pas d'image`}
                  alt="film"
                  className="card-img-top"
                />
              </Link>
            </div>
            <div className="card-footer d-flex justify-content-between">
              <h6 className="align-self-center mb-0">{film.original_title}</h6>
              <span className="text-blue font-italic mb-0">
                Note : {film.vote_average}
              </span>
            </div>
          </div>
        </div>
      );
};
export default FilmItem;
