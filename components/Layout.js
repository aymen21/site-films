import Head from 'next/head';
import Navbar from './Navbar';

/**
 * L'interface du site 
 */
const Layout = (props) => (
  <div>
    <Head>
      <title>Accueil - Films</title>
      <link rel="stylesheet" href="https://bootswatch.com/4/cerulean/bootstrap.min.css"/>
    </Head>
    <Navbar/>
    <div className="container">
      {props.children}
    </div>
  </div>
);

export default Layout;